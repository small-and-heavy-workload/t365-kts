<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        a{
            text-decoration: none;
            display: inline-block;
            line-height: 30px;
        }
    </style>
</head>
<body>
<h1>
    user模块
</h1>

<p>
    <a href="user.action?methodName=add">添加用户</a> <br>
    <a href="user.action?methodName=update">修改用户</a> <br>
    <a href="user.action?methodName=del">删除用户</a> <br>
    <a href="user.action?methodName=query">查询用户列表</a> <br>
</p>

<p>
    <a href="goods.action?methodName=getGoodsById&id=365">查询商品信息</a> <br>
</p>
</body>
</html>
