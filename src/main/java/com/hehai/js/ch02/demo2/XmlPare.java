package com.hehai.js.ch02.demo2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;

public class XmlPare {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        //添加
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document parse = builder.parse("src/main/resources/example/ch02/demo2/收藏信息.xml");
//        Element brand = parse.createElement("Brand");
//        brand.setAttribute("name","三星");
//        Element type = parse.createElement("Type");
//        type.setAttribute("name","NoteX");
//        brand.appendChild(type);
//        Element element=(Element) parse.getElementsByTagName("PhoneInfo").item(0);
//        element.appendChild(brand);

        //修改
//        NodeList list= parse.getElementsByTagName("Brand");
//        for (int i = 0; i < list.getLength(); i++) {
//            Element element1=(Element) list.item(i);
//            String name = element1.getAttribute("name");
//            if(name.equals("三星")){
//                element1.setAttribute("name","SAMSUNG");
//            }
//        }

        //删除
        NodeList list = parse.getElementsByTagName("Brand");
        for (int i = 0; i < list.getLength(); i++) {
            Element element1 = (Element) list.item(i);
            String name = element1.getAttribute("name");
            if (name.equals("SAMSUNG")) {
                element1.getParentNode().removeChild(element1);
            }
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(parse);
        transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        StreamResult result1 = new StreamResult(new FileOutputStream("src/main/resources/example/ch02/demo2/收藏信息.xml"));
        transformer.transform(domSource, result1);
    }
}
