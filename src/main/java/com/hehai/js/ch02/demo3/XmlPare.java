package com.hehai.js.ch02.demo3;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class XmlPare {
    public static void main(String[] args) throws DocumentException, IOException {
        //查询
        SAXReader saxReader = new SAXReader();
        Document read = saxReader.read(new File("src/main/resources/example/ch02/demo2/收藏信息.xml"));
        Element rootElement = read.getRootElement();
//        for (Iterator i= rootElement.elementIterator(); i.hasNext();) {
//            Element element=(Element) i.next();
//            System.out.println(element.attributeValue("name"));
//            for (Iterator j= element.elementIterator(); j.hasNext();) {
//                Element element1=(Element) j.next();
//                System.out.println(element1.attributeValue("name"));
//
//            }
//        }

        //添加
//        Element age=rootElement.addElement("Brand");
//        age.addAttribute("name", "learnigdoem");


        //修改节点
//        Element element=rootElement.element("Brand");
//        Attribute name = element.attribute("name");
//        name.setText("lewo");

        //删除
        Attribute name = rootElement.attribute("Brand");
        rootElement.remove(name);


        OutputFormat format = OutputFormat.createCompactFormat();
        format.setEncoding("utf-8");
        XMLWriter xmlWriter = new XMLWriter(new FileWriter("src/main/resources/example/ch02/demo2/收藏信息.xml"), format);
        xmlWriter.write(read);
        xmlWriter.close();
    }
}
