package com.hehai.js.ch02.demo1;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class XmlPare {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document parse = builder.parse("src/main/resources/example/ch02/demo2/收藏信息.xml");
        NodeList list = parse.getElementsByTagName("Brand");
        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            Element element=(Element) item;
            String name = element.getAttribute("name");
            NodeList childNodes = element.getChildNodes();
            for (int j = 0; j < childNodes.getLength(); j++) {
                Node item1 = childNodes.item(j);
                if (item1.getNodeType()==Node.ELEMENT_NODE){
                    Element element1=(Element) item1;
                    String name1 = element1.getAttribute("name");
                    System.out.println(name1);
                }
            }
        }
    }
}
