package com.hehai.js.ch01.demo4;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

public class SimpleBeanConvertor {

    /**
     * 将HTTP请求中的参数赋值给指定类型JavaBean的相关属性
     *
     * @param request    HTTP请求
     * @param targetType 目标JavaBean类型
     * @return targetType 类型的实例
     */
    public static <T> T Convert(HttpServletRequest request, Class<T> targetType) throws InstantiationException, IllegalAccessException, InvocationTargetException, UnsupportedTypeException {
        //获取所有请求参数
        Map<String, String[]> params = request.getParameterMap();
        //判断集合中有没有元素
        if (params.size() == 0) return null;
        //创建JavaBean实例对象
        T target = targetType.newInstance();
        //获取JavaBean中所有方法
        Method[] allMethods = targetType.getMethods();
        //判断是JavaBean中否存在有方法
        if (allMethods != null && allMethods.length > 0) {
            //遍历元素
            for (Method method : allMethods) {
                //判断是否已set开头的方法
                if (method.getName().startsWith("set")) {
                    //获取参数列表
                    Class<?>[] args = method.getParameterTypes();
                    //仅处理单参情况
                    if (args.length == 1) {
                        //获取set方法对应的请求参数:setName+name
                        String paramName = method.getName().substring(3, 4).toLowerCase()
                                + method.getName().substring(4);
                        //请求中有此参数
                        if (params.containsKey(paramName)) {
                            try {
                                //将请求参数值转换为setter方法参数类型
                                Object value = parseValue(params.get(paramName), args[0]);
                                method.invoke(target, value);
                            } catch (ParseException e) {
                                System.out.println("参数转换错误:" + e);
                                throw new RuntimeException(e);
                            }
                        }
                    } else if (Boolean.class == args[0] || boolean.class == args[0]) {
                        //如果是boolean,不存在表示false
                        method.invoke(target, false);
                    }
                }
            }
        }
        return target;
    }

    private static Object parseValue(String[] value, Class type) throws ParseException, UnsupportedTypeException {
        if (String.class == type)
            return value[0];
        if (java.util.Date.class == type)
            return new SimpleDateFormat("yyyy-MM-dd").parse(value[0]);
        if (boolean.class == type || Boolean.class == type) {
            if (value.length > 0) {
                return Boolean.parseBoolean(value[0]);
            } else {
                return null;
            }
        }
        if (char.class == type || Character.class == type)
            return value[0].charAt(0);
        if (short.class == type || byte.class == type || long.class == type || float.class == type || double.class == type) {
            //将值类型变更为对应的包装类型
            try {
                type = Class.forName("java.lang." + type.getName().substring(0, 1).toUpperCase() + type.getName().substring(1));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
            if (type.getName().startsWith("java.lang.") || type.getName().startsWith("java.math.")) {
                try {
                    return type.getConstructor(String.class).newInstance(value[0]);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                } catch (InstantiationException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (NoSuchMethodException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        if(Array.class==type){
            return type.getComponentType();
        }
        if (type.isArray())
            return Integer.parseInt(value[0]);
        throw new UnsupportedTypeException();
    }
}
