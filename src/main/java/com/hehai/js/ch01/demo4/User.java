package com.hehai.js.ch01.demo4;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private int id;
    private String username;
    private Boolean ok;
    private char sex;
    private Date birthday;
    private float salary;

    public User() {
    }

    public User(Integer id, String username, Boolean ok, char sex, Date birthday, float salary) {
        this.id = id;
        this.username = username;
        this.ok = ok;
        this.sex = sex;
        this.birthday = birthday;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", ok=" + ok +
                ", sex=" + sex +
                ", birthday=" + birthday +
                ", salary=" + salary +
                '}';
    }
}
