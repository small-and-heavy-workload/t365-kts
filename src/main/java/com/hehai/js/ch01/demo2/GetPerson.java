package com.hehai.js.ch01.demo2;

import com.hehai.js.ch01.demo1.Person;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GetPerson {
    public static void main(String[] args) throws NoSuchMethodException, InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchFieldException {
        // 1.private构造方法取值与赋值
        Class<Person> personClass = Person.class;
        Person person = personClass.newInstance();
        Constructor<Person> declaredConstructor = personClass.getDeclaredConstructor(String.class);
        declaredConstructor.setAccessible(true);
        person = declaredConstructor.newInstance("小明");
        System.out.println("构造方法为name赋值:" + person);

        //2.public属性取值赋值
        Field message = personClass.getField("message");
        message.set(person, "ok");
        System.out.println("message属性赋值:" + message.get(person));

        //3.protected方法取值赋值
        Method address = personClass.getDeclaredMethod("setAddress", String.class);
        address.setAccessible(true);
        address.invoke(person, "第一职中");
        Method getAddress = personClass.getDeclaredMethod("getAddress");
        getAddress.setAccessible(true);
        Object invoke = getAddress.invoke(person);
        System.out.println(invoke);
    }
}
