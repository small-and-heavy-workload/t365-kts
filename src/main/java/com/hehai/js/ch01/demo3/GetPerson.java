package com.hehai.js.ch01.demo3;

import com.hehai.js.ch01.demo1.Person;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GetPerson {
    public static void main(String[] args) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        //1.实例方法赋值调用
        Class<Person> personClass = Person.class;
        Person person = personClass.newInstance();
        Method setName = personClass.getMethod("setName", String.class);
        setName.invoke(person,"小美");
        Method getName = personClass.getMethod("getName");
        Object invoke = getName.invoke(person);
        System.out.println("实例方法:"+invoke);

        //2.静态方法调用
        Method show = personClass.getMethod("show");
        show.invoke(null);
    }
}
