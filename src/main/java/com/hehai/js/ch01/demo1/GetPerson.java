package com.hehai.js.ch01.demo1;

import com.hehai.example.ch01.demo1.Person;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class GetPerson {
    public static void main(String[] args) throws NoSuchMethodException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Class<Person> personClass = Person.class;

        //1.类名
        String simpleName = personClass.getSimpleName();
        System.out.println("类名:" + simpleName);

        //2.修饰符
        int modifiers = personClass.getModifiers();
        System.out.println("修饰符:" + modifiers);

        //3.继承关系
        Class<? super Person> superclass = personClass.getSuperclass();
        System.out.println("继承关系:" + superclass);

        //4.构造方法
        Constructor<Person> constructor = personClass.getConstructor();
        System.out.println("构造方法:" + constructor);

        //5.属性
        Field message = personClass.getField("message");
        System.out.println("属性:" + message);

        //6.方法
        Method getName = personClass.getMethod("getName");
        System.out.println("方法" + getName);
    }
}
