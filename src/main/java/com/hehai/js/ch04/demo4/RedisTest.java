package com.hehai.js.ch04.demo4;

import com.hehai.example.ch04.demo1.JedisApI;

public class RedisTest {
    public static void main(String[] args) {
        JedisApI jedisApI=new JedisApI();
        //添加
        jedisApI.set("no2","小明");
        //查询
        System.out.println(jedisApI.get("no2"));
        //删除
        jedisApI.delete("no2");
        System.out.println(jedisApI.get("no2"));
    }
}
