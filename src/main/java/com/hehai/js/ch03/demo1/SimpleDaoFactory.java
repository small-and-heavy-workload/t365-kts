package com.hehai.js.ch03.demo1;

public class SimpleDaoFactory {

    public static NewsDao calculateResult(String operator) {
        switch (operator) {
            case "+":
                return new PlusNewsDaoImpl();
            case "-":
                return new ReduceNewsDaoImpl();
            case "*":
                return new RideNewsDaoImpl();
            case "/":
                return new ExceptNewsDaoImpl();
            default:
                throw new RuntimeException("运算符不存在");
        }
    }
}
