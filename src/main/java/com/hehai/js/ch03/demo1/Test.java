package com.hehai.js.ch03.demo1;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String operator="";
        System.out.print("输入第一个数字:");
        double number1 = in.nextDouble();
        System.out.print("输入第二个数字:");
        double number2 = in.nextDouble();
        System.out.print("输入运算符:");
        operator = in.next();
        NewsDao newsDao=SimpleDaoFactory.calculateResult(operator);
        double calculate = newsDao.calculate(number1, number2, operator);
        System.out.println("计算结果:"+calculate);
    }
}
