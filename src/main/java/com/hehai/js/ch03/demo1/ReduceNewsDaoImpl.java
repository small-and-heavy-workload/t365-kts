package com.hehai.js.ch03.demo1;

public class ReduceNewsDaoImpl implements NewsDao {
    @Override
    public double calculate(double number1, double number2, String operator) {
        return number1 - number2;
    }
}
