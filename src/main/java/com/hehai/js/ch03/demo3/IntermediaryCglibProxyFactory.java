package com.hehai.js.ch03.demo3;


import net.sf.cglib.proxy.Enhancer;

public class IntermediaryCglibProxyFactory {

    private static IntermediaryMethodInterceptor callback = new IntermediaryMethodInterceptor();

    /**
     * 创建代理对象
     * @param target
     * @return
     * @param <T>
     */
    public static <T> T create(Class<T> target){
        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(callback);
        enhancer.setSuperclass(target);
        return (T) enhancer.create();
    }
}
