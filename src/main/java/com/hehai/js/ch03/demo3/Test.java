package com.hehai.js.ch03.demo3;

import com.hehai.js.ch03.demo1.NewsDao;
import com.hehai.js.ch03.demo1.ReduceNewsDaoImpl;
import com.hehai.js.ch03.demo1.SimpleDaoFactory;
import com.hehai.js.ch03.demo2.AbstractFactory;
import com.hehai.js.ch03.demo2.NewExsNewsImpl;

public class Test {
    public static void main(String[] args) {
        NewDao newExsNews = IntermediaryCglibProxyFactory.create(SNewImpl.class);
        newExsNews.save("小山");
    }
}
