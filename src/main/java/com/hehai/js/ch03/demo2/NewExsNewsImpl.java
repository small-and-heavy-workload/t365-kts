package com.hehai.js.ch03.demo2;

import com.hehai.js.ch03.demo1.EXsNewsDaoImpl;
import com.hehai.js.ch03.demo1.NewsDao;

public class NewExsNewsImpl implements AbstractFactory{
    @Override
    public NewsDao getInstance() {
        return new EXsNewsDaoImpl();
    }
}
