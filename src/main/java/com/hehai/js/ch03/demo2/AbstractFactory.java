package com.hehai.js.ch03.demo2;


import com.hehai.js.ch03.demo1.NewsDao;

public interface AbstractFactory {
     NewsDao getInstance();
}
