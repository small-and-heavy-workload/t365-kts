package com.hehai.js.ch03.demo2;

import com.hehai.js.ch03.demo1.EXsNewsDaoImpl;
import com.hehai.js.ch03.demo1.NewsDao;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String operator="";
        System.out.print("输入第一个数字:");
        double number1 = in.nextDouble();
        System.out.print("输入第二个数字:");
        double number2 = in.nextDouble();
        System.out.print("输入运算符:");
        operator = in.next();
        AbstractFactory abstractFactory=new NewExsNewsImpl();
        NewsDao instance = abstractFactory.getInstance();
        System.out.println("计算结果:"+instance.calculate(number1,number2,operator));
    }
}
