package com.hehai.example.ch02.demo1;

/**
 * @author llq
 * @Date 2024 03 27 15 01
 * 单例对象
 */
public class Singleton {
    private static Singleton singleton;

    private Singleton(){

    }
    public static Singleton getInstance(){
        if(null==singleton){
            singleton=new Singleton();
        }
        return singleton;
    }

}
