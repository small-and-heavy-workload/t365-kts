package com.hehai.example.ch02.demo1;

/**
 * @author llq
 * @Date 2024 03 27 15 05
 * 多线程运行
 */
public class MyThread extends Thread {
    public void run() {
        // 线程运行的代码逻辑
        System.out.println("Hello, I am a new thread!");
        Singleton instance = Singleton.getInstance();
        Singleton instance2 = Singleton.getInstance();
        System.out.println("90");
        System.out.println(instance);
        System.out.println(instance2);
        int i=1;
        while (true){
            i=i+1;
            System.out.println("iiiiiiii===="+i);
            if(instance!=instance2){
                return;
            }
        }
    }

    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        MyThread2 myThread2 = new MyThread2();
        myThread2.start();
        myThread.start(); // 启动线程
    }
}
