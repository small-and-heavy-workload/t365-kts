package com.hehai.example.ch02.demo1;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class XMLParse {
    Document doc = null;

    //获取DOM树
    public void getDocument() {
        //1.获取解析器工厂
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //2.获取解析器
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            //3.加载DOM树
            doc = builder.parse("src/main/resources/example/ch02/demo2/收藏信息.xml");
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        }
    }

    public void showInfo() {
        //获取<book>的所有节点集合
        NodeList nodeList = doc.getElementsByTagName("book");
        for (int i = 0; i < nodeList.getLength(); i++) {
            //i=0时得到第一个book节点
            Node book = nodeList.item(i);
            Element bookEle = (Element) book;
            //读取此节点的name属性
            String branStr = bookEle.getAttribute("id");
            //得到book节点下的子节点
            NodeList types = bookEle.getChildNodes();
            for (int j = 0; j < types.getLength(); j++) {
                //得到book节点下的第一个子节点
                Node type = types.item(j);
                if (type.getNodeType() == Node.ELEMENT_NODE) {
                    Element typeEle = (Element) type;
                    String typeStr = typeEle.getFirstChild().getNodeValue();
                    System.out.println(branStr + ":" + typeStr);
                }
            }
        }
    }

    public static void main(String[] args) {
        XMLParse parse = new XMLParse();
        parse.getDocument();
        System.out.println();

        parse.showInfo();
    }
}
