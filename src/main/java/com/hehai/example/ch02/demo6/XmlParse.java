package com.hehai.example.ch02.demo6;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;

public class XmlParse {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, TransformerException {

        //创建dom树
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse("src/main/resources/example/ch02/demo2/收藏信息.xml");

        //删除指定数据
        NodeList list = document.getElementsByTagName("Brand");
        for (int i=0;i<list.getLength();i++){
            Element element = (Element) list.item(i);
            String brandName = element.getAttribute("name");
            if(brandName.equals("SAMSUNG")){
                document.getParentNode().removeChild(element);
            }
        }

        //保存修改
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource =new DOMSource(document);
        transformer.setOutputProperty(OutputKeys.ENCODING,"utf-8");
        StreamResult streamResult = new StreamResult(new FileOutputStream("src/main/resources/example/ch02/demo2/收藏信息.xml"));
        transformer.transform(domSource,streamResult);
    }
}
