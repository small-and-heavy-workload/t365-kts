package com.hehai.example.ch02.demo2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class XmlParse {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        //1.得到DOm解析器的工厂实例
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //2.从Dom工厂获得dom解析器
        DocumentBuilder db=factory.newDocumentBuilder();
        //3.解析xml文档，得到一个Document,即Dom树
        Document doc = db.parse("src/main/resources/example/ch02/demo2/收藏信息.xml");
        //4.读取新闻
        NodeList list = doc.getElementsByTagName("pubdate");
        Element element =(Element) list.item(0);
        //5.读取文本节点
        String nodeValue = element.getFirstChild().getNodeValue();
        NodeList list2 = doc.getElementsByTagName("pubdate");
        Element element2 =(Element) list.item(0);
        System.out.println(nodeValue);
    }
}
