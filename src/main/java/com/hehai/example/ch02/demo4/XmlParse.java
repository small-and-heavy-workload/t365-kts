package com.hehai.example.ch02.demo4;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;

public class XmlParse {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        //1.得到dom解析器的工厂实例
        DocumentBuilderFactory factory =DocumentBuilderFactory.newInstance();
        //2.从dom工厂获得dom解析器
        DocumentBuilder db = factory.newDocumentBuilder();
        //3.解析xml文档，得到一个document,即dom树
        Document doc=db.parse("sr/main/resources/example/ch02/demo2/收藏信息.xml");
        //创建Brand节点
        Element brand = doc.createElement("Brand");
        brand.setAttribute("name","三星");
        //创建type节点
        Element type = doc.createElement("type");
        type.setAttribute("name","Notex");
        //添加父子关系
        brand.appendChild(type);
        Element element =(Element) doc.getElementsByTagName("PhoneInfo").item(0);
        element.appendChild(brand);
        //保存xml文件
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //设置编码类型
        transformer.setOutputProperty(OutputKeys.ENCODING,"utf-8");
        StreamResult streamResult = new StreamResult(new FileOutputStream("src/main/resources/example/ch02/demo2/收藏信息.xml"));
        //把Dom树转换为xml文件
        transformer.transform(domSource,streamResult);
    }
}
