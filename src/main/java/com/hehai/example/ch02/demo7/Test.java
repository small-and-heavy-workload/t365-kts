package com.hehai.example.ch02.demo7;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;

public class Test {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        //1.创建dom树
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document parse = builder.parse("src/main/resources/example/ch02/demo2/收藏信息.xml");

        //2.创建节点
        Element brand = parse.createElement("Brand");
        brand.setAttribute("name", "三星");
        brand.setTextContent("三星最新款api");
        Node phoneInfo = parse.getElementsByTagName("PhoneInfo").item(0);
        Element element = (Element) phoneInfo;
        element.appendChild(brand);

        //3.保存创建节点信息
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(parse);
        transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        StreamResult res = new StreamResult(new FileOutputStream("src/main/resources/example/ch02/demo2/收藏信息.xml"));
        transformer.transform(source, res);
    }
}
