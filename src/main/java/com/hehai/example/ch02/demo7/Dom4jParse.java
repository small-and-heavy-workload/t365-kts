package com.hehai.example.ch02.demo7;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.Iterator;


public class Dom4jParse {
    public static void main(String[] args) {
        showPhoneInfo();
    }

    public static void showPhoneInfo(){
        Document doc=null;
        try{
            //加载Dom树
            SAXReader saxResult=new SAXReader();
            doc=saxResult.read(new File("src/main/resources/example/ch02/demo2/收藏信息.xml"));
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
        //获取XML的根节点
        Element root=doc.getRootElement();
        //遍历所有的Brand标签
        for (Iterator iterator=root.elementIterator();iterator.hasNext();){
            Element brandEle=(Element) iterator.next();
            //输出Brand标签的name属性值
            System.out.println("品牌:"+brandEle.attributeValue("name"));
            //遍历Type标签
            for (Iterator itType=brandEle.elementIterator();itType.hasNext();){
                Element next = (Element) itType.next();
                System.out.println("型号:"+next.attributeValue("name"));
            }
        }
    }
}
