package com.hehai.example.ch03.demo1.service.impl;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class IntermediaryMethodInterceptor implements MethodInterceptor {
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        before();
        Object object = methodProxy.invokeSuper(o, objects);
        after();
        return "看房记录：买家反馈" + object + "";
    }

    private void after() {
        System.out.println("前期准备");
        System.out.println("查找房源");
        System.out.println("和卖家沟通时间");
    }

    private void before() {
        System.out.println("后期跟踪");
        System.out.println("和买家沟通意见");
    }
}
