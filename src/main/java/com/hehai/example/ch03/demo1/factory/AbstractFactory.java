package com.hehai.example.ch03.demo1.factory;

import com.hehai.example.ch03.demo1.dao.NewsDao;

public interface AbstractFactory {
    public NewsDao getInstance();
}
