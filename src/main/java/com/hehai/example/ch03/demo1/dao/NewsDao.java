package com.hehai.example.ch03.demo1.dao;

import com.hehai.example.ch03.demo1.pojo.News;

public interface NewsDao {

    public void save(News news);
}
