package com.hehai.example.ch03.demo1.factory.impl;

import com.hehai.example.ch03.demo1.dao.NewsDao;
import com.hehai.example.ch03.demo1.factory.AbstractFactory;
import com.hehai.example.ch03.demo1.service.impl.newsDaoRedisImpl;

public class MySqlDaoFactory implements AbstractFactory {
    @Override
    public NewsDao getInstance() {
        return new newsDaoRedisImpl();
    }
}
