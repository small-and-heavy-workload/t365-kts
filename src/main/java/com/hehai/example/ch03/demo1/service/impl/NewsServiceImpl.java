package com.hehai.example.ch03.demo1.service.impl;

import com.hehai.example.ch03.demo1.dao.NewsDao;
import com.hehai.example.ch03.demo1.dao.impl.NewsDaoImpl;
import com.hehai.example.ch03.demo1.factory.SimpleDaoFactory;
import com.hehai.example.ch03.demo1.pojo.News;
import com.hehai.example.ch03.demo1.service.NewsService;

public class NewsServiceImpl implements NewsService {

    //1.简单工厂
//    NewsDao newsDao = SimpleDaoFactory.getInstance();

    //2.依赖倒置
    NewsDao newsDao;

    public void setNewsDao(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    @Override
    public void save(News news) {
        newsDao.save(news);
    }

}
