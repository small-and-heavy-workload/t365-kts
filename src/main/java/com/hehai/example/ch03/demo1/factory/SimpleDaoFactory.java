package com.hehai.example.ch03.demo1.factory;

import com.hehai.example.ch03.demo1.dao.impl.NewsDaoImpl;
import com.hehai.example.ch03.demo1.dao.NewsDao;
import com.hehai.example.ch03.demo1.service.impl.NewDaoOracleImpl;
import com.hehai.example.ch03.demo1.service.impl.newsDaoRedisImpl;

public class SimpleDaoFactory {

    public static NewsDao getInstance() {
        return new NewsDaoImpl();
    }

    public static NewsDao getInstance(String key) {
        switch (key) {
            case "mysql":
                return new NewsDaoImpl();
            case "oracle":
                return new NewDaoOracleImpl();
            case "redis":
                return new newsDaoRedisImpl();
            default:
                throw new RuntimeException("无效的数据库类型:" + key + ",DAO获取失败");
        }
    }
}
