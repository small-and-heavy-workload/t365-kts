package com.hehai.example.ch03.demo1.service.impl;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class IntermediaryInvocationHandler implements InvocationHandler {
    private Object target;

    public void setTarget(Object target) {
        this.target = target;
    }

    private void after() {
        System.out.println("前期准备");
        System.out.println("查找房源");
        System.out.println("和卖家沟通时间");
    }

    private void before() {
        System.out.println("后期跟踪");
        System.out.println("和买家沟通意见");
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException {
        before();
        Object feedback = method.invoke(target, args);
        after();
        return "看房记录：买家反馈" + feedback + "";
    }
}
