package com.hehai.example.ch03.demo1.service;

import com.hehai.example.ch03.demo1.pojo.News;

public interface NewsService {

    public void save(News news);

}
