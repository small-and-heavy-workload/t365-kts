package com.hehai.example.ch03.demo1.factory.impl;

import com.hehai.example.ch03.demo1.service.impl.IntermediaryInvocationHandler;

import java.lang.reflect.Proxy;

public class IntermediaryJdkProxyFactory {

    public static <T> T create(Object target) {
        IntermediaryInvocationHandler handler = new IntermediaryInvocationHandler();
        handler.setTarget(target);
        return (T) Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), handler);
    }
}
