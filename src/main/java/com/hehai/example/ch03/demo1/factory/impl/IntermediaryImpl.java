package com.hehai.example.ch03.demo1.factory.impl;

import com.hehai.example.ch03.demo1.factory.Buyer;

public class IntermediaryImpl implements Buyer {

    private Buyer target;

    public IntermediaryImpl(Buyer target) {
        this.target = target;
    }

    @Override
    public String havealook() {
        before();
        String feedback = target.havealook();
        after();
        return "看房记录：买家反馈" + feedback + "";
    }

    private void after() {
        System.out.println("前期准备");
        System.out.println("查找房源");
        System.out.println("和卖家沟通时间");
    }

    private void before() {
        System.out.println("后期跟踪");
        System.out.println("和买家沟通意见");
    }
}
