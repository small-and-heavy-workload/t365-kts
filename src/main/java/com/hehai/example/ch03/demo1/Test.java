package com.hehai.example.ch03.demo1;

import com.hehai.example.ch03.demo1.dao.NewsDao;
import com.hehai.example.ch03.demo1.factory.SimpleDaoFactory;
import com.hehai.example.ch03.demo1.pojo.News;
import com.hehai.example.ch03.demo1.service.impl.NewsServiceImpl;

public class Test {
    public static void main(String[] args) {
        NewsDao dao = SimpleDaoFactory.getInstance();
        NewsServiceImpl service = new NewsServiceImpl();
//        service.setNewsDao(dao);

        News news = new News();
        news.setNtitle("测试标题2");
        news.setNcontent("测试内容2");
        service.save(news);
    }
}
