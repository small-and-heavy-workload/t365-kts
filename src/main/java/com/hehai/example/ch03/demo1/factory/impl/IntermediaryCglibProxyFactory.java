package com.hehai.example.ch03.demo1.factory.impl;

import com.hehai.example.ch03.demo1.service.impl.IntermediaryMethodInterceptor;
import net.sf.cglib.proxy.Enhancer;

public class IntermediaryCglibProxyFactory {
    private static IntermediaryMethodInterceptor callback=new IntermediaryMethodInterceptor();

    public static <T> T create(Class<T> target){
        Enhancer enhancer=new Enhancer();
        enhancer.setCallback(callback);
        enhancer.setSuperclass(target);
        return (T)enhancer.create();
    }
}
