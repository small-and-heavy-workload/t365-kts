package com.hehai.example.ch03.demo1.dao.impl;

import com.hehai.example.ch03.demo1.dao.NewsDao;
import com.hehai.example.ch03.demo1.pojo.News;

public class NewsDaoImpl implements NewsDao {

    @Override
    public void save(News news) {
        System.out.println("保存新闻信息到数据库"+news);
    }
}
