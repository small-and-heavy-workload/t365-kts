package com.hehai.example.ch03.demo1;

import com.hehai.example.ch03.demo1.dao.NewsDao;
import com.hehai.example.ch03.demo1.factory.AbstractFactory;
import com.hehai.example.ch03.demo1.factory.Buyer;
import com.hehai.example.ch03.demo1.factory.SimpleDaoFactory;
import com.hehai.example.ch03.demo1.factory.impl.*;
import com.hehai.example.ch03.demo1.pojo.News;
import com.hehai.example.ch03.demo1.service.impl.NewsServiceImpl;
import org.junit.Test;

public class test1 {

    @Test
    public void addNews() throws Exception {
//        NewsDao dao = SimpleDaoFactory.getInstance("mysql");
        AbstractFactory factory = new MySqlDaoFactory();
        NewsDao dao = factory.getInstance();
        NewsServiceImpl service = new NewsServiceImpl();
        service.setNewsDao(dao);

        News news = new News();
        news.setNtitle("测试标题2");
        news.setNcontent("测试内容2");
        service.save(news);
    }

    @Test
    public void havealookImpl() throws Exception{
        Buyer buyer=new IntermediaryImpl(new RealBuyer());
        String havealook = buyer.havealook();
        System.out.println(havealook);
    }

    @Test
    public void havealookjdkProxy() throws Exception{
        Buyer buyer= IntermediaryJdkProxyFactory.create(new RealBuyer());

        String result=buyer.havealook();
        System.out.println(result);
    }

    @Test
    public void havelookCglibProxy() throws Exception{
        RealBuyer buyer = IntermediaryCglibProxyFactory.create(RealBuyer.class);
        String havealook = buyer.havealook();
        System.out.println(havealook);
    }
}
