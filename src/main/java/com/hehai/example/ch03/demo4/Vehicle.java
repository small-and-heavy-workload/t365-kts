package com.hehai.example.ch03.demo4;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class Vehicle implements InvocationHandler {


    private Object object;

    public void setObject(Object object) {
        this.object = object;
    }

    private void before(){
        System.out.println("查看车子");
    }

    private void after(){
        System.out.println("看了又看");
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object invoke = method.invoke(object,args);
        after();
        return "决定购买"+invoke+"了";
    }
}
