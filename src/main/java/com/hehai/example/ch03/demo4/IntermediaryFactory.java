package com.hehai.example.ch03.demo4;

import com.hehai.example.ch03.demo1.service.impl.IntermediaryInvocationHandler;

import java.lang.reflect.Proxy;

public class IntermediaryFactory {

    public static <T> T create(Object target){
        Vehicle  handler=new Vehicle();

        handler.setObject(target);

        return (T) Proxy.newProxyInstance(target.getClass().getClassLoader(),target.getClass().getInterfaces(),handler);
    }
}
