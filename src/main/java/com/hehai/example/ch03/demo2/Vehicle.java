package com.hehai.example.ch03.demo2;

public class Vehicle {
    private String name;
    private Vehs vehs;

    public Vehicle(Vehs name) {
        this.vehs =name;
    }

    public String show(){
        vehs.run();
        System.out.println("查看车子");
        System.out.println("看了又看");
        return "决定购买"+name+"了";
    }

    public void setName(String name) {
        this.name = name;
    }
}
