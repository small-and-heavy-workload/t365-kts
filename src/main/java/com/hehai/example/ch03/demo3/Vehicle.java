package com.hehai.example.ch03.demo3;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class Vehicle implements MethodInterceptor {

    private void before(){
        System.out.println("查看车子");
    }

    private void after(){
        System.out.println("看了又看");
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        before();
        Object object = methodProxy.invokeSuper(o, objects);
        after();
        return "决定购买"+object+"了";
    }
}
