package com.hehai.example.ch03.demo3;

import net.sf.cglib.proxy.Enhancer;

public class IntermediaryCglibProxyFactory {
    private static Vehicle vehicle = new Vehicle();

    public static <T> T create(Class<T> target){
        Enhancer enhancer =new Enhancer();
        enhancer.setCallback(vehicle);
        enhancer.setSuperclass(target);

        return (T) enhancer.create();
    }
}
