package com.hehai.example.ch04.demo1;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;

public class JedisApI {
    private static JedisPool jedisPool;

    static {
        //创建并设置连接池配置对象
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(50);   //连接池最大连接数，默认为8
        config.setMaxIdle(10);    //连接池中的最大空闲连接，默认为8
        config.setMinIdle(7);     //连接池中的最小空闲连接，默认为0
        config.setMaxWaitMillis(1000);  //获取资源的等待时间
        config.setTestOnBorrow(true);  //获取资源时否验证资源的有效性

        //创建Jedis连接池
        jedisPool = new JedisPool(config,  //连接池配置对象
                "192.168.130.133",   //Redis服务器地址
                6379,       //Redis服务端口
                10000,      //连接超时时间，单位毫秒，默认2000ms
                "123056",   //Redis密码
                0       //数据库索引
        );
    }

    public void destroy() {
        if (!(jedisPool == null || jedisPool.isClosed())) jedisPool.close();
    }


    public boolean set(String key, String value) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            //返还到连接池
            if (jedis != null) jedis.close();
        }
    }

    public boolean set(String key, int seconds, String value) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.setex(key, seconds, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return false;
    }

    public boolean exist(String key) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis.exists(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return false;
    }

    public String get(String key) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return value;
    }

    public Long ttl(String key) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis.ttl(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return (long) -2;
    }

    public void delete(String key) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.del(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
    }

    //list添加
    public Long lpush(String key, String... values) {
        Long value = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.lpush(key, values);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return value;
    }

    //list删除
    public Long lrem(String key, Long index, String values) {
        Long value = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.lrem(key, index, values);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return value;
    }

    //list修改
    public String lset(String key, Long index, String values) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.lset(key, index, values);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return value;
    }

    //list过期时间
    public Long expire(String keys) {
        Long value = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.expire(keys, 60);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return value;
    }

    //map添加
    public Long hset(String keys,String key,String values) {
        Long value = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.hset(keys,key,values);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) jedis.close();
        }
        return value;
    }


}
