package com.hehai.example.ch01.demo9;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AccessPersonMethods {
    public static void main(String[] args) {
        try {
            Class clz = Class.forName("com.hehai.example.ch01.demo1.Person");
            Method getAge = clz.getDeclaredMethod("getAge", null);
            getAge.setAccessible(true);
            getAge.invoke(null, null);
            System.out.println("年龄是:" + getAge);
            Object person = clz.newInstance();
            Method silentMethod = clz.getDeclaredMethod("silentMethod", null);
            silentMethod.setAccessible(true);
            silentMethod.invoke(person, null);
            Method setName = clz.getDeclaredMethod("setName", String.class);
            setName.invoke(person, "New Person");
            Object returName = clz.getDeclaredMethod("getName").invoke(person);
            System.out.println("刚才设定的name是:" + returName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
