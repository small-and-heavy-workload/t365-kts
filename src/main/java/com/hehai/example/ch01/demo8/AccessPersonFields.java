package com.hehai.example.ch01.demo8;

import java.lang.reflect.Field;

public class AccessPersonFields {
    public static void main(String[] args) {
        try {
            Class cls=Class.forName("com.hehai.example.ch01.demo1.Person");
            Object person =cls.newInstance();
            Field name =cls.getDeclaredField("name");
            name.setAccessible(true);
            System.out.println("赋值前的name:"+name.get(person));
            name.set(person,"New Person");
            System.out.println("赋值后的name:"+name.get(person));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
