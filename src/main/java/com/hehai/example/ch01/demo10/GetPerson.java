package com.hehai.example.ch01.demo10;

import com.hehai.example.ch01.demo1.Person;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GetPerson {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        //获取Person的Class对象
        Class<Person> personClass = Person.class;
        //通过Class对象创建Person对象
        Person person = personClass.newInstance();
        //调用Person的setName方法,并传入参数,返回值
        Object invoke = personClass.getMethod("setName", String.class).invoke(person, "张三");
        System.out.println(invoke);

        //设置私有属性
        // 获取 name 字段
        Field field = personClass.getDeclaredField("name");
        // 设置为可访问（如果字段是私有的）
        field.setAccessible(true);
        // 设置 person 对象上 name 字段的值为 "小山"
        field.set(person, "小山");
        // 获取并打印 person 对象上 name 字段的值
        System.out.println(field.get(person));

        //获取所有方法
        Method[] methods = personClass.getDeclaredMethods();
        //遍历方法
        for (Method method : methods) {
            //获取方法名,返回值类型和参数数量
            System.out.println("方法名:" + method.getName() + ",返回值类型:" + method.getReturnType() + ",参数数量:" + method.getParameterCount());
            //获取方法参数类型
            Class<?>[] parameterTypes = method.getParameterTypes();
            //遍历参数类型
            for (Class<?> parameterType : parameterTypes) {
                System.out.println("参数名:" + parameterType.getName());
            }
        }
    }
}
