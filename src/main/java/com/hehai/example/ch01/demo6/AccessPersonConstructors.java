package com.hehai.example.ch01.demo6;

public class AccessPersonConstructors {
    public static void main(String[] args) {
        try {
            Class clz = Class.forName("com.hehai.example.ch01.demo1.Person");
            Object obj = clz.newInstance();  // new新建 Instance实例
            System.out.println(obj);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
