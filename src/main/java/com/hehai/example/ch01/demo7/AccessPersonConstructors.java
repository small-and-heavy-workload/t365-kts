package com.hehai.example.ch01.demo7;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class AccessPersonConstructors {
    public static void main(String[] args) {
        //测试反射调用构造方法
        try {
            Class clz = Class.forName("com.hehai.example.ch01.demo1.Person");
            Object obj = clz.newInstance();
            System.out.println(obj);

            //获取Person的无参构造
            Constructor c1 = clz.getDeclaredConstructor();
            // Person的无参构造为public,这里可以直接访问
            obj = c1.newInstance();
            System.out.println(obj);

            //获取Person的单参构造
            Constructor c2 = clz.getDeclaredConstructor(String.class);
            c2.setAccessible(true);
            obj = c2.newInstance("New Person");
            System.out.println(obj);

            Constructor c3 = clz.getDeclaredConstructor(String.class, String.class, String.class);
            c3.setAccessible(true);
            obj = c3.newInstance("New Person", "beijing", "hello!");
            System.out.println(obj);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}
