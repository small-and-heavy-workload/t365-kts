package com.hehai.example.ch01.demo5;

import com.hehai.example.ch01.demo1.Person;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class GetClassMethodsInfo {
    public static void main(String[] args) {
        Method[] methods = Person.class.getDeclaredMethods();

        System.out.println("==========方法展示==========");
        for (Method method : methods) {
            System.out.println("方法名:" + method.getName());
            System.out.println("返回值类型:" + method.getReturnType().getName());

            // 获取方法参数列表
            Class[] params = method.getParameterTypes();
            if (params.length == 0) {
                System.out.println("该方法没有参数");
            } else {
                System.out.println("该方法的参数列表为:[");
                for (int i = 0; i < params.length; i++) {
                    if (i != 0)
                        System.out.println(",");
                    System.out.println(params[i].getName());
                }
                System.out.println("]");
            }

            System.out.println("访问修饰符:");
            int modifiers = method.getModifiers();
            //判断该方法的访问修饰符
            if ((modifiers & Modifier.PUBLIC) == Modifier.PUBLIC)
                System.out.println("public");
            else if ((modifiers & Modifier.PROTECTED) == Modifier.PROTECTED)
                System.out.println("protected");
            else if ((modifiers & Modifier.PRIVATE) == Modifier.PRIVATE)
                System.out.println("private");
            else
                System.out.println("default(package)");

            //判断该属性是否有static修饰符
            if ((modifiers & Modifier.STATIC) == Modifier.STATIC)
                System.out.println("这是一个静态方法");
            //判断该属性是否有final修饰符
            if ((modifiers & Modifier.FINAL) == Modifier.FINAL)
                System.out.println("这是一个final方法");
            //判断该方法是否有abstract修饰符
            if ((modifiers & Modifier.ABSTRACT) == Modifier.ABSTRACT)
                System.out.println("这是一个抽象方法");
            //判断该方法是否有synchronized修饰符
            if ((modifiers & Modifier.SYNCHRONIZED) == Modifier.SYNCHRONIZED)
                System.out.println("这是一个同步谅");

            //获取方法所属的类或接口的Class实例
            Class declaringClass = method.getDeclaringClass();
            System.out.println("方法声明在:" + declaringClass.getName());

            //获取方法抛出的异常类型，即throws子句中声明的异常
            Class[] exceptions = method.getExceptionTypes();
            if (exceptions.length > 0) {
                System.out.println("该方法抛出的异常有:[");
                for (int i = 0; i < exceptions.length; i++) {
                    if (i != 0)
                        System.out.println(",");
                    System.out.println(exceptions[i].getName());
                }
                System.out.println("]");
            }
        }
    }
}
