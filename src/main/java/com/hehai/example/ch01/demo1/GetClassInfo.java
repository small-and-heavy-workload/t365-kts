package com.hehai.example.ch01.demo1;

import java.lang.reflect.Modifier;

public class GetClassInfo {
    public static void main(String[] args) {
        Class clz = Person.class; //获取Class实例
        String fullName = clz.getName(); //得到类的完全限定名(包名+类名)
        String simpleName = clz.getSimpleName();  //得到类名
        System.out.println("以下是" + fullName + "类的基本信息");
        System.out.println("--------------------");

        //获取Person类所在的包
        Package pkg = clz.getPackage();
        System.out.println(simpleName + "定义在:" + pkg.getName() + "包中");

        Class superClz = clz.getSuperclass();  //获取父类
        System.out.println(simpleName + "类的超类是:" + superClz.getName());
        System.out.println("--------------------");

        Class[] interfaces = clz.getInterfaces();  //实现接口
        System.out.println(simpleName + "类所实现接口:");
        for (Class c : interfaces) {
            System.out.println("\t" + c.getName());
        }

        System.out.println("--------------------");
        int modifiers = clz.getModifiers();  //获取修饰符
        System.out.println(simpleName + "类的修饰符是:");
        if(((modifiers & Modifier.PUBLIC) == Modifier.PUBLIC)) {
            System.out.println("\t访问修饰符是:public");
        } else {
            System.out.println("\t访问修饰符是:default(package)");
        }
        if((modifiers & Modifier.FINAL) == Modifier.FINAL)
            System.out.println("\t这个类是final的");
        if((modifiers & Modifier.ABSTRACT) == Modifier.ABSTRACT)
            System.out.println("\t这是一个抽象类");
        if((modifiers & Modifier.INTERFACE) == Modifier.INTERFACE)
            System.out.println("\t这是一个接口");
        System.out.println("--------------------");
    }
}
