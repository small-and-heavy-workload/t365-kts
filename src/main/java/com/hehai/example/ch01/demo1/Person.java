package com.hehai.example.ch01.demo1;

import java.io.IOException;
import java.io.Serializable;

public final class Person extends BaseClass implements Serializable {
    //成员变量
    private String name;
    static final int age = 30;
    protected String address;
    public String message;

    public String getName() {
        return name;
    }

    public String setName(String name) {
        return name;
    }

    static final int getAge() {
        return age;
    }

    protected String getAddress() {
        return address;
    }

   private void silentMethod() throws IOException, NullPointerException {
       System.out.println("这是悄悄话");
   }

   public Person() {

   }

   private Person(String name) {
        this.name = name;
   }

   protected Person(String name, String address, String message) {
        this.name = name;
        this.address = address;
        this.message = message;
   }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
