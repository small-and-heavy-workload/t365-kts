package com.hehai.example.ch01.demo1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

public class GetClassConstructorsInfo {
    public static void main(String[] args) {
        Constructor[] cons = Person.class.getDeclaredConstructors();

        System.out.println("======构造方法展示======");
        for (Constructor con : cons) {
            System.out.println("访问修饰符:");
            int modifiers = con.getModifiers();
            //判断该构造方法的访问修饰符
            if((modifiers & Modifier.PUBLIC) == Modifier.PUBLIC)
                System.out.println("public");
            else if ((modifiers & Modifier.PROTECTED) == Modifier.PROTECTED)
                System.out.println("protected");
            else if ((modifiers & Modifier.PRIVATE) == Modifier.PRIVATE)
                System.out.println("private");
            else
                System.out.println("default(package)");

            // 获取构造方法的参数列表
            Class[] params = con.getParameterTypes();
            if(params.length == 0) {
                System.out.println("该构造方法没有参数");
            } else {
                System.out.println("该构造方法的参数列表为:[");
                for (int i = 0; i < params.length; i++) {
                    if(i != 0)
                        System.out.print(",");
                    System.out.print(params[i].getName());
                }
                System.out.println("]");
            }
            System.out.println("--------------------");
        }
    }
}
