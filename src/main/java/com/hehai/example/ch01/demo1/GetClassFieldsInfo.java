package com.hehai.example.ch01.demo1;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class GetClassFieldsInfo {
    public static void main(String[] args) {
        Field[] fields = Person.class.getDeclaredFields();

        //展示属性的一些信息
        System.out.println("======属性展示======");
        for (Field field : fields) {
            System.out.println("属性名:" + field.getName());
            System.out.println("类型:" + field.getType().getName());
            System.out.println("访问修饰符:");
            int modifiers = field.getModifiers();
            if((modifiers & Modifier.PUBLIC) == Modifier.PUBLIC)
                System.out.println("public");
            else if ((modifiers & Modifier.PROTECTED) == Modifier.PROTECTED)
                System.out.println("protected");
            else if ((modifiers & Modifier.PRIVATE) == Modifier.PRIVATE)
                System.out.println("private");
            else
                System.out.println("default(package)");

            //判断该属性是否有static修饰符
            if((modifiers & Modifier.STATIC) == Modifier.STATIC)
                System.out.println("这是一个静态属性");
            //判断该属性是否有final修饰符
            if((modifiers & Modifier.FINAL) == Modifier.FINAL)
                System.out.println("这是一个final属性");
        }
    }
}
