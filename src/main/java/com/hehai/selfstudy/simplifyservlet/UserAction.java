package com.hehai.selfstudy.simplifyservlet;

import com.alibaba.fastjson.JSON;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class UserAction extends BaseAction{

    public void add(HttpServletRequest request, HttpServletResponse response){
        System.out.println("--------------->user.add");
    }
    public void del(HttpServletRequest request, HttpServletResponse response){
        System.out.println("--------------->user.del");
    }
    public void update(HttpServletRequest request, HttpServletResponse response){
        System.out.println("--------------->user.update");
    }

    public void query(HttpServletRequest request, HttpServletResponse response) throws IOException {

        List<String> userList =new ArrayList<>();
        userList.add("zhangsan");
        userList.add("lisi");
        userList.add("wangwu");

        PrintWriter out = response.getWriter();

        out.print(JSON.toJSONString(userList));
        out.flush();
        out.close();
    }
}
