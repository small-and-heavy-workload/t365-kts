package com.hehai.selfstudy.simplifyservlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("*.action")
public class DispatcherServlet extends HttpServlet {

    private Map<String,BaseAction>map=new HashMap<>();

    @Override
    public void init() throws ServletException {
        //存储路径
        UserAction userAction=new UserAction();
        GoodsAction goodsAction=new GoodsAction();
        map.put("/user",userAction);
        map.put("/goods",goodsAction);
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String url= request.getRequestURI();
        //截取*.action中 *.前面的路径
        url=url.substring(url.lastIndexOf("/"),url.lastIndexOf("."));

        //查询map中是否有路径
        BaseAction action=map.get(url);
        //调用对应对象
        action.execute(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
