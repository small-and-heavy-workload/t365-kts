package com.hehai.selfstudy.simplifyservlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoodsAction extends BaseAction{

    public void getGoodsById(HttpServletRequest request, HttpServletResponse response){
        String goodsId =request.getParameter("id") ;
        System.out.println("执行service层按id查找的方法");
        System.out.println("---------------goods.getGoodsById"+goodsId);
    }
}
