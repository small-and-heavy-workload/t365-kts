package com.hehai.selfstudy.simplifyservlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BaseAction{

    public void execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            //获取methodName为参数赋值的对应方法
            Method method=this.getClass().getDeclaredMethod(request.getParameter("methodName"),
                    HttpServletRequest.class,HttpServletResponse.class);
            method.setAccessible(true);
            method.invoke(this,request,response);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
