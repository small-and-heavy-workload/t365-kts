package com.hehai.jd.ch01.demo1;

import com.hehai.jd.ch01.demo1.pojo.PlaneFlight;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Iterator;

public class Test {
    public static void main(String[] args) {
        try {
            // 创建一个SAXReader对象
            SAXReader reader = new SAXReader();
            // 使用reader解析XML文档
            Document document = reader.read(new File("D:\\KTSP\\反射\\ T365-kts\\src\\main\\resources\\jd\\ch01\\demo1\\airlines.xml"));
            // 获取根元素
            Element root = document.getRootElement();

            // 获取所有item节点的迭代器
            Iterator<Element> itemIterator = root.elementIterator("AirlinesTime");

            while (itemIterator.hasNext()) {
                // 遍历每个item节点
                Element itemElement = itemIterator.next();

                // 获取并打印name和value子节点的文本内容
                Element nameElement = itemElement.element("id");
                Element valueElement = itemElement.element("Company");

                if (nameElement != null && valueElement != null) {
                    System.out.println("Name: " + nameElement.getText());
                    System.out.println("Value: " + valueElement.getText());
                }
            }

        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
}
