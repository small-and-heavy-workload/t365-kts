package com.hehai.jd.ch01.demo1.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlaneFlight {
    private Integer Id;
    private String company;
    private String airlineCode;
    private String startDrome;
    private String arriveDrome;
    private String startTime;
    private String arriveTime;
    private String  mode;
    private Integer  airlineStop;
    private String week;

}
