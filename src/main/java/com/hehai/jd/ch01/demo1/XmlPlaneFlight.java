package com.hehai.jd.ch01.demo1;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.w3c.dom.NodeList;
import sun.security.jca.GetInstance;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

public class XmlPlaneFlight<T> {


    private static XmlPlaneFlight xmlPlaneFlight;
    private static Document doc = null;

    private XmlPlaneFlight() {
    }

    /**
     * 创建对象
     *
     * @return
     */
    public static XmlPlaneFlight getInstance() {
        if (xmlPlaneFlight == null) {
            xmlPlaneFlight = new XmlPlaneFlight();
        }
        return xmlPlaneFlight;
    }

    /**
     * 创建Document对象
     *
     * @param file
     */
    public void getCreateDocument(String file) {
        //1.创建Document对象
        SAXReader reader = new SAXReader();
        try {
            doc = reader.read(new File(file));
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取xml方法
     *
     * @param param
     * @return
     */
    public List<T> get(String param, Class<T> type) {
        List<T> list = new ArrayList<>();
        // 获取xml文档的根元素
        Element root = doc.getRootElement();
        // 获取根元素下指定名称的子元素迭代器
        Iterator<Element> iterator = root.elementIterator(param);
        T entity = null;
        while (iterator.hasNext()) {
            // 获取下一个子元素
            Element itemElement = iterator.next();
            entity = createInstanceAndPopulate(itemElement, type);
            if (entity != null) {
                list.add(entity);
            }
        }
        return list;
    }


    /**
     * 获取xml map方法
     *
     * @param param
     * @return
     */
    public Map<T,T> getmap(String param, Class<T> type) {
        Integer i=0;
        Map<T,T> map = new HashMap<>();
        // 获取xml文档的根元素
        Element root = doc.getRootElement();
        // 获取根元素下指定名称的子元素迭代器
        Iterator<Element> iterator = root.elementIterator(param);
        T entity = null;
        while (iterator.hasNext()) {
            // 获取下一个子元素
            Element itemElement = iterator.next();
            entity = createInstanceAndPopulate(itemElement, type);
            String name = itemElement.getName();
            i++;
            if (entity != null) {
                map.put((T)(name+i).toString(),entity);
            }
        }
        return map;
    }


    private T createInstanceAndPopulate(Element itemElement, Class<T> type) {
        T entity = null;
        try {
            // 获取T类型的构造方法
            Constructor<T> constructor = type.getConstructor();
            // 创建T类型的实例
            entity = constructor.newInstance();

            // 获取JavaBean的所有属性
            Field[] fields = type.getDeclaredFields();
            for (Field field : fields) {
                // 设置访问权限，以便能够访问私有属性
                field.setAccessible(true);

                // 获取属性名称，并转换为setter方法名（假设遵循JavaBean命名规范）
                String propertyName = field.getName();
                String ps = propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
                String setterName = "set" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);

                try {
                    // 尝试在Element中查找与属性名匹配的元素
                    Element propertyElement = itemElement.element(ps);
                    if (propertyElement != null) {
                        // 获取属性值，并转换为适当的类型
                        Object value = convertValue(propertyElement.getText(), field.getType());

                        // 如果值不为null，则调用setter方法设置属性值
                        if (value != null) {
                            type.getMethod(setterName, field.getType()).invoke(entity, value);
                        }
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return entity;
    }


    private Object convertValue(String text, Class<?> targetType) throws Exception {
        if (targetType == Boolean.class || targetType == boolean.class) {
            return Boolean.parseBoolean(text);
        } else if (targetType == Integer.class || targetType == int.class) {
            return Integer.parseInt(text);
        } else if (targetType == Long.class || targetType == long.class) {
            return Long.parseLong(text);
        } else if (targetType == Double.class || targetType == double.class) {
            return Double.parseDouble(text);
        } else if (targetType == Float.class || targetType == float.class) {
            return Float.parseFloat(text);
        } else if (targetType == Date.class) {
            return new SimpleDateFormat("yyyy-MM-dd").parse(text);
        } else if (targetType == String.class) {
            return text;
        } else {
            // 如果需要支持其他类型，可以在这里添加转换逻辑
            throw new RuntimeException("Unsupported target type: " + targetType.getName());
        }
    }
}
