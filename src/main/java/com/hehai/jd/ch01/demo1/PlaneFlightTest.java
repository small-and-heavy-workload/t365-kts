package com.hehai.jd.ch01.demo1;

import com.hehai.jd.ch01.demo1.pojo.PlaneFlight;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PlaneFlightTest {
    public static void main(String[] args) {
        XmlPlaneFlight xmlPlaneFlight=XmlPlaneFlight.getInstance();
        xmlPlaneFlight.getCreateDocument("src/main/resources/jd/ch01/demo1/airlines.xml");
//        List<PlaneFlight> airlinesTime = xmlPlaneFlight.get("AirlinesTime",PlaneFlight.class);
//        for (PlaneFlight planeFlight : airlinesTime) {
//            System.out.println(planeFlight);
//        }

        Map<String,PlaneFlight> airlinesTime = xmlPlaneFlight.getmap("AirlinesTime", PlaneFlight.class);
//        Set<Map.Entry<String, PlaneFlight>> entries = airlinesTime.entrySet();
//        for (Map.Entry<String, PlaneFlight> entry : entries) {
//            String key = entry.getKey();
//            PlaneFlight value = entry.getValue();
//            System.out.println("Key: " + key + ", Value: " + value);
//        }

//        for (String key : airlinesTime.keySet()) {
//            PlaneFlight value = airlinesTime.get(key);
//            System.out.println("Key: " + key + ", Value: " + value);
//        }



        Iterator<Map.Entry<String, PlaneFlight>> iterator = airlinesTime.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, PlaneFlight> entry = iterator.next();
            String key = entry.getKey();
            PlaneFlight value = entry.getValue();
            System.out.println("Key: " + key + ", Value: " + value);
        }


//        System.out.println(airlinesTime.get("AirlinesTime2"));

    }
}
