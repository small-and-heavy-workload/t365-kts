package com.hehai.jd.ch01.test;


import org.dom4j.Element;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class XmlParser<T> {

    public T parse(Element rootElement, Class<T> type) {
        T entity = null;
        try {
            // 创建T类型的实例
            entity = createInstanceAndPopulate(rootElement, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    private T createInstanceAndPopulate(Element rootElement, Class<T> type) throws Exception {
        T entity = null;
        try {
            // 获取T类型的无参构造方法并创建实例
            Constructor<T> constructor = type.getDeclaredConstructor();
            constructor.setAccessible(true);
            entity = constructor.newInstance();

            // 获取JavaBean的所有属性
            Field[] fields = type.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true); // 设置访问权限以访问私有属性

                // 获取属性名称，并转换为setter方法名（假设遵循JavaBean命名规范）
                String propertyName = field.getName();
                String setterName = "set" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);

                try {
                    // 尝试在Element中查找与属性名匹配的元素
                    Element propertyElement = rootElement.element(propertyName);
                    if (propertyElement != null) {
                        // 获取属性值，并转换为适当的类型
                        Object value = convertValue(propertyElement.getText(), field.getType());

                        // 如果值不为null，则调用setter方法设置属性值
                        if (value != null) {
                            invokeSetter(entity, setterName, value, field.getType());
                        }
                    }
                } catch (NoSuchMethodException e) {
                    // 处理找不到setter方法的情况
                    System.err.println("No setter method found for field: " + propertyName);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Error parsing XML or converting value", e);
        }
        return entity;
    }

    private void invokeSetter(Object target, String methodName, Object value, Class<?> paramType) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Method setter = target.getClass().getMethod(methodName, paramType);
        setter.invoke(target, value);
    }

    private Object convertValue(String text, Class<?> targetType) {
        if (targetType == String.class) {
            return text;
        } else if (targetType == Integer.class || targetType == int.class) {
            try {
                return Integer.parseInt(text);
            } catch (NumberFormatException e) {
                System.err.println("Cannot convert text to integer: " + text);
                return null;
            }
        } else if (targetType == Boolean.class || targetType == boolean.class) {
            return Boolean.parseBoolean(text);
        } else {
            System.err.println("Unsupported type for conversion: " + targetType.getName());
            return null;
        }
    }
}
