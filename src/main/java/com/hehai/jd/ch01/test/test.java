package com.hehai.jd.ch01.test;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Test;

public class test {
    private XmlParser<Person> xmlParser;
    private Document document;


    @Test
    public void testCreateInstanceAndPopulate() throws DocumentException {
        xmlParser = new XmlParser<>();

        // 使用SAXReader来解析XML文件
        SAXReader reader = new SAXReader();
        document = reader.read("src/main/resources/jd/ch01/demo1/test.xml"); // 替换为你的XML文件路径
        Element rootElement = document.getRootElement();
        Person person = xmlParser.parse(rootElement, Person.class);
        System.out.println(person);
    }
}
