package com.hehai.jd.ch01.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class test1 {
    public static void main(String[] args) {
        // 创建一个Date对象
        Date date = new Date();

        // 创建一个SimpleDateFormat对象，并指定时间格式
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");

// 设置默认的日期部分（例如：今天的日期）
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date()); // 使用当前日期
        calendar.set(Calendar.HOUR_OF_DAY, 8); // 设置小时
        calendar.set(Calendar.MINUTE, 25); // 设置分钟
        calendar.set(Calendar.SECOND, 0); // 设置秒数为0
        calendar.set(Calendar.MILLISECOND, 0); // 设置毫秒数为0

// 使用format方法将Date对象转换为指定格式的字符串（这里实际上不需要，因为我们已经有了Date对象）
// String formattedDate = formatter.format(calendar.getTime());

// 如果您确实需要将Date对象转换回字符串，可以使用以下代码：
        String formattedDate = formatter.format(calendar.getTime());

// 输出格式化后的日期字符串
        System.out.println("Formatted Date: " + formattedDate);

// 如果您需要的是Date对象而不是字符串，可以直接使用calendar.getTime()
        Date parsedDate = calendar.getTime();
        System.out.println("Parsed Date: " + parsedDate);
    }
}
